from django.urls import path
from . import views
# from .views import LoginView

urlpatterns = [
    path('', views.index, name='home'),
    path('about/', views.about, name='about'),
    path('lectors/', views.lector, name='lector'),
    path('lessons/', views.lesson, name='lesson'),
    path('tasks/', views.task, name='task'),
    path('add_lector/', views.add_lector, name='add_lector'),
    path('add_lesson/', views.add_lesson, name='add_lesson'),
    path('add_task/', views.add_task, name='add_task'),
    path('lectors/<int:pk>', views.LectorDetailsView.as_view(), name='lector_detail'),
    path('lessons/<int:pk>', views.LessonDetailsView.as_view(), name='lesson_detail'),
    path('tasks/<int:pk>', views.TaskDetailsView.as_view(), name='task_detail'),
    path('lectors/<int:pk>/update', views.LectorUpdateView.as_view(), name='lector_update'),
    path('lessons/<int:pk>/update', views.LessonUpdateView.as_view(), name='lesson_update'),
    path('tasks/<int:pk>/update', views.TaskUpdateView.as_view(), name='task_update'),
    path('lectors/<int:pk>/delete', views.LectorDeleteView.as_view(), name='lector_delete'),
    path('lessons/<int:pk>/delete', views.LessonDeleteView.as_view(), name='lesson_delete'),
    path('tasks/<int:pk>/delete', views.TaskDeleteView.as_view(), name='task_delete'),
    path('registration/', views.register_request, name='registration'),
    path('login/', views.login_request, name='login'),
    path('logout/', views.logout_request, name='logout')
]
