from .models import Lector, Predmets, Task
from django.forms import ModelForm, TextInput, NumberInput, Textarea
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms


class LectorForm(ModelForm):
    class Meta:
        model = Lector
        fields = ['first_name', 'last_name', 'age', 'learn_lesson']

        widgets = {
            "first_name": TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Ім'я"
            }),
            "last_name": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Прізвище'
            }),
            "age": NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'Вік'
            }),
            "learn_lesson": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Список предметів'
            })
        }


class LessonForm(ModelForm):
    class Meta:
        model = Predmets
        fields = ['lesson',]
        widgets = {
            "lesson": TextInput(attrs={
                "class": "form-control",
                "placeholder": "Назва предмету"
            }),
        }


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ['name', "description", 'lesson']
        widgets = {
            "name": TextInput(attrs={
                "class": "form-control",
                "placeholder": "Завдання"
            }),
            "description": Textarea(attrs={
                "class": "form-control",
                "placeholder": "Опис"
            }),
            "lesson": TextInput(attrs={
                "class": "form-control",
                "placeholder": "Предмет"
            }),
        }


class RegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("first_name", "last_name", "username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user
