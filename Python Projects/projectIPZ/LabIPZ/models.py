from django.db import models


class Lector(models.Model):
    first_name = models.CharField(
        "Ім'я викладача",
        max_length=50,
    )
    last_name = models.CharField(
        'Прізвище викладача',
        max_length=50,
    )
    age = models.IntegerField(
        'Вік викладача',
    )
    learn_lesson = models.ForeignKey(
        'Predmets',
        related_name='+',
        on_delete=models.SET_NULL,
        null=True,
    )

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    def get_absolute_url(self):
        return f'/lectors/{self.id}'

    class Meta:
        verbose_name = 'Викладач'
        verbose_name_plural = 'Викладачі'


class Predmets(models.Model):
    lesson = models.CharField(
        "Назва предмету",
        max_length=50,
    )

    def __str__(self):
        return self.lesson

    def get_absolute_url(self):
        return f'/lessons/{self.id}'

    class Meta:
        verbose_name = 'Предмет',
        verbose_name_plural = 'Предмети'


class Task(models.Model):
    name = models.CharField(
        "Назва предмету",
        max_length=100,
    )
    description = models.TextField(
        "Опис"
    )
    lesson = models.ForeignKey(
        'Predmets',
        related_name='+',
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return f'/tasks/{self.id}'

    class Meta:
        verbose_name = 'Завдання'
        verbose_name_plural = 'Декілька завдань'
