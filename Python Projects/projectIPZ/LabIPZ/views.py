from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from django.shortcuts import render, redirect
from django.views.generic import UpdateView, DetailView, DeleteView, CreateView
from .forms import LectorForm, LessonForm, RegistrationForm, TaskForm
from django.contrib.auth.forms import AuthenticationForm
from .models import Lector, Predmets, Task


def index(request):
    return render(request, 'LabIPZ/index.html')


def about(request):
    return render(request, 'LabIPZ/about.html')


def lector(request):
    lector_list = Lector.objects.all()
    return render(request, 'LabIPZ/lector.html', {'lector_list': lector_list})


def lesson(request):
    lesson_list = Predmets.objects.all()
    return render(request, 'LabIPZ/lesson.html', {'lesson_list': lesson_list})


def task(request):
    task_list = Task.objects.all()
    return render(request, 'LabIPZ/tasks/task.html', {'task_list': task_list})


class LessonDetailsView(DetailView):
    model = Predmets
    template_name = 'LabIPZ/lessons_details.html'
    context_object_name = 'lesson_detail'


class LectorDetailsView(DetailView):
    model = Lector
    template_name = 'LabIPZ/lectors_details.html'
    context_object_name = 'lector_detail'


class TaskDetailsView(DetailView):
    model = Task
    template_name = 'LabIPZ/tasks/tasks_details.html'
    context_object_name = 'task_detail'


class LectorUpdateView(UpdateView):
    model = Lector
    template_name = 'LabIPZ/add_lector.html'
    form_class = LectorForm


class LessonUpdateView(UpdateView):
    model = Predmets
    template_name = 'LabIPZ/add_lesson.html'
    form_class = LessonForm


class TaskUpdateView(UpdateView):
    model = Task
    template_name = 'LabIPZ/tasks/add_task.html'
    form_class = TaskForm


class LessonDeleteView(DeleteView):
    model = Predmets
    success_url = '/lessons/'
    template_name = 'LabIPZ/lesson_delete.html'


class LectorDeleteView(DeleteView):
    model = Lector
    success_url = '/lectors/'
    template_name = 'LabIPZ/lector_delete.html'


class TaskDeleteView(DeleteView):
    model = Task
    success_url = '/tasks/'
    template_name = 'LabIPZ/tasks/task_delete.html'


def add_lector(request):
    error = ''
    if request.method == 'POST':
        form = LectorForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('lector')
        else:
            error = 'Некоректні дані'

    form = LectorForm()
    data = {
        'form': form,
        'error': error
    }
    return render(request, 'LabIPZ/add_lector.html', data)


def add_lesson(request):
    error_lesson = ''
    if request.method == 'POST':
        form_lesson = LessonForm(request.POST)
        if form_lesson.is_valid():
            form_lesson.save()
            return redirect('lesson')
        else:
            error_lesson = 'Некоректні дані'

    form_lesson = LessonForm()
    data = {
        'form_lesson': form_lesson,
        'error_lesson': error_lesson
    }
    return render(request, 'LabIPZ/add_lesson.html', data)


def add_task(request):
    error_task = ''
    if request.method == 'POST':
        form_task = TaskForm(request.POST)
        if form_task.is_valid():
            form_task.save()
            return redirect('task')
        else:
            error_task = 'Некоректні дані'

    form_task = TaskForm()
    data = {
        'form_task': form_task,
        'error_task': error_task
    }
    return render(request, 'LabIPZ/tasks/add_task.html', data)


def register_request(request):
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Успішна Реєстрація")
            return redirect("home")
        messages.error(request, "Введені неправильні дані. Спробуйте ще раз.")
    form = RegistrationForm()
    return render(request=request, template_name="LabIPZ/registration_form.html", context={"register_form": form})


def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f'Ви увійшли як {username}')
                return redirect("home")
            else:
                messages.error(request, "Некоректний логін або пароль")
        else:
            messages.error(request, "Некоректний логін або пароль")
    form = AuthenticationForm()
    return render(request=request, template_name="LabIPZ/login.html", context={"login_form": form})


def logout_request(request):
    logout(request)
    messages.info(request, "Успішний вихід із акаунту")
    return redirect('home')
