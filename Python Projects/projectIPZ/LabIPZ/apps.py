from django.apps import AppConfig


class LabipzConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'LabIPZ'
