from django.contrib import admin
from .models import Lector, Predmets, Task

admin.site.register(Lector)
admin.site.register(Predmets)
admin.site.register(Task)
